FROM rails:4.1.6

MAINTAINER Ivan Kolmychek <ivan.kolmychek@gmail.com> 

# https://github.com/docker-library/rails/issues/13
RUN apt-get update && apt-get install -y postgresql-client mysql-client sqlite3 --no-install-recommends && rm -rf /var/lib/apt/lists/*

EXPOSE 3000